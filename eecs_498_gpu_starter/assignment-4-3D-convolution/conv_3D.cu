#include <wb.h>

#define wbCheck(stmt)                                                     \
  do {                                                                    \
    cudaError_t err = stmt;                                               \
    if (err != cudaSuccess) {                                             \
      wbLog(ERROR, "CUDA error: ", cudaGetErrorString(err));              \
      wbLog(ERROR, "Failed to run stmt ", #stmt);                         \
      return -1;                                                          \
    }                                                                     \
  } while (0)

//@@ Define any useful program-wide constants here
#define MASK_WIDTH 3
#define MASK_RADIUS 1
#define TILE_SIZE 4
#define BLOCK_SIZE (TILE_SIZE + MASK_WIDTH - 1)


//@@ Define constant memory for device kernel here
__constant__ float deviceMask[MASK_WIDTH * MASK_WIDTH * MASK_WIDTH];

__global__ void conv3d(float *input, float *output, const int z_size,
                       const int y_size, const int x_size) {

   int tx = threadIdx.x; 
   int ty = threadIdx.y; 
   int tz = threadIdx.z;
   int row_o = blockIdx.y*TILE_SIZE + ty; 
   int col_o = blockIdx.x*TILE_SIZE + tx; 
   int z_o = blockIdx.z*TILE_SIZE + tz; 
   int row_i = row_o - MASK_WIDTH/2;
   int col_i = col_o - MASK_WIDTH/2;
   int z_i = z_o - MASK_WIDTH/2;
      
   __shared__ float N_ds[TILE_SIZE+MASK_WIDTH-1][TILE_SIZE+MASK_WIDTH-1][TILE_SIZE+MASK_WIDTH-1];
   
   // Copy Input data from global memory to shared memory
   if ((row_i >= 0) && (row_i < y_size) && (col_i >= 0) && (col_i < x_size) && (z_i >= 0) && (z_i < z_size)) {
       N_ds[tz][ty][tx] = input[z_i*(y_size*x_size) + row_i*x_size + col_i];
   }
   else {
       // Halo elements set to 0
       N_ds[tz][ty][tx] = 0;
   }
   __syncthreads();
  
   float temp_out = 0;
   
   if ((tx < TILE_SIZE) && (ty < TILE_SIZE) && (tz < TILE_SIZE)) {
       for (int z = 0; z < MASK_WIDTH; ++z) {
           for (int y = 0; y < MASK_WIDTH; ++y) {
               for (int x = 0; x < MASK_WIDTH; ++x) {
                   temp_out += deviceMask[z*(MASK_WIDTH*MASK_WIDTH) + y*MASK_WIDTH+x]*N_ds[z+tz][y+ty][x+tx];
               } // for 
           } // for 
       } // for 
   
       if ((row_o < y_size) && (col_o < x_size) && (z_o < z_size)){
           output[z_o*(y_size*x_size) + row_o*x_size+col_o] = temp_out;       
       }
   } // if
}

int main(int argc, char *argv[]) {
  wbArg_t args;
  int z_size;
  int y_size;
  int x_size;
  int inputLength, kernelLength;
  float *hostInput;
  float *hostKernel;
  float *hostOutput;
  float *deviceInput;
  float *deviceOutput;

  args = wbArg_read(argc, argv);

  // Import data
  hostInput = (float *)wbImport(wbArg_getInputFile(args, 0), &inputLength);
  hostKernel =
      (float *)wbImport(wbArg_getInputFile(args, 1), &kernelLength);
  hostOutput = (float *)malloc(inputLength * sizeof(float));

  // First three elements are the input dimensions
  z_size = hostInput[0];
  y_size = hostInput[1];
  x_size = hostInput[2];
  wbLog(TRACE, "The input size is ", z_size, "x", y_size, "x", x_size);
  assert(z_size * y_size * x_size == inputLength - 3);
  assert(kernelLength == 27);

  wbTime_start(GPU, "Doing GPU Computation (memory + compute)");

  wbTime_start(GPU, "Doing GPU memory allocation");
  //@@ Allocate GPU memory here
  // Recall that inputLength is 3 elements longer than the input data
  // because the first  three elements were the dimensions
  cudaMalloc(&deviceInput, z_size * y_size * x_size * sizeof(float));
  cudaMalloc(&deviceOutput, z_size * y_size * x_size * sizeof(float));
  wbTime_stop(GPU, "Doing GPU memory allocation");

  wbTime_start(Copy, "Copying data to the GPU");
  //@@ Copy input and kernel to GPU here
  // Recall that the first three elements of hostInput are dimensions and
  // do not need to be copied to the gpu
  // Copy Kernel to the constant Memory
  cudaMemcpyToSymbol(deviceMask, hostKernel, MASK_WIDTH*MASK_WIDTH*MASK_WIDTH*sizeof(float));
  cudaMemcpy(deviceInput, &hostInput[3], z_size * y_size * x_size * sizeof(float),cudaMemcpyHostToDevice);
  wbTime_stop(Copy, "Copying data to the GPU");
  
  wbTime_start(Compute, "Doing the computation on the GPU");
  //@@ Initialize grid and block dimensions here
  dim3 bd(BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
  dim3 gd(ceil(x_size/(float)TILE_SIZE), ceil(y_size/(float)TILE_SIZE), ceil(z_size/(float)TILE_SIZE));
  conv3d <<< gd, bd >>> (deviceInput, deviceOutput, z_size, y_size, x_size);  

  //@@ Launch the GPU kernel here
  cudaDeviceSynchronize();
  wbTime_stop(Compute, "Doing the computation on the GPU");

  wbTime_start(Copy, "Copying data from the GPU");
  cudaMemcpy(&hostOutput[3], deviceOutput, z_size * y_size * x_size * sizeof(float),cudaMemcpyDeviceToHost);
  //@@ Copy the device memory back to the host here
  // Recall that the first three elements of the output are the dimensions
  // and should not be set here (they are set below)
  wbTime_stop(Copy, "Copying data from the GPU");

  wbTime_stop(GPU, "Doing GPU Computation (memory + compute)");

  // Set the output dimensions for correctness checking
  hostOutput[0] = z_size;
  hostOutput[1] = y_size;
  hostOutput[2] = x_size;
  wbSolution(args, hostOutput, inputLength);

  // Free device memory
  cudaFree(deviceInput);
  cudaFree(deviceOutput);

  // Free host memory
  free(hostInput);
  free(hostOutput);
  return 0;
}
