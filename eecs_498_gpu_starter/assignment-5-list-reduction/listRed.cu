// Given a list (lst) of length n
// Output its sum = lst[0] + lst[1] + ... + lst[n-1];

#include <wb.h>

#define BLOCK_SIZE 512 //@@ You can change this
#define KERNEL_OPT 7

#define wbCheck(stmt)                                                     \
  do {                                                                    \
    cudaError_t err = stmt;                                               \
    if (err != cudaSuccess) {                                             \
      wbLog(ERROR, "Failed to run stmt ", #stmt);                         \
      wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));      \
      return -1;                                                          \
    }                                                                     \
  } while (0)

// Reduction #1 Kernel
__global__ void total_red1(float *input, float *output, int len) {
  //@@ Load a segment of the input vector into shared memory
  //@@ Traverse the reduction tree
  //@@ Write the computed sum of the block to the output vector at the
  //@@ correct index
  __shared__ float sdata[BLOCK_SIZE];
  int t_id = threadIdx.x;
  int i = threadIdx.x + blockDim.x * blockIdx.x;
  sdata[t_id] = input[i];
  __syncthreads();
  
  if (i < len) 
  { 
      for (int x = 1; x < blockDim.x; x=x*2) 
      {
          if (t_id %(2*x) == 0) {
             sdata[t_id] += sdata[t_id+x];
          }
          __syncthreads();
      }
      if (t_id == 0) output[blockIdx.x] = sdata[0];
  }
}

// Reduction #7 Kernel
__global__ void total_red7(float *input, float *output, int len) {
  //@@ Load a segment of the input vector into shared memory
  //@@ Traverse the reduction tree
  //@@ Write the computed sum of the block to the output vector at the
  //@@ correct index
  volatile __shared__ float sdata[BLOCK_SIZE];
  unsigned int t_id = threadIdx.x;
  unsigned int i = t_id + (BLOCK_SIZE*2) * blockIdx.x;
  unsigned int gridSize = BLOCK_SIZE*2*gridDim.x;
  sdata[t_id] = 0;
  
  while (i < len) {
    sdata[t_id] += input[i] + input[i+BLOCK_SIZE];
    i += gridSize;
  }
  __syncthreads();

  if (BLOCK_SIZE >= 1024) { if (t_id < 512) { sdata[t_id] += sdata[t_id + 512]; } __syncthreads();}
  if (BLOCK_SIZE >= 512) { if (t_id < 256) { sdata[t_id] += sdata[t_id + 256]; } __syncthreads();}
  if (BLOCK_SIZE >= 256) { if (t_id < 128) { sdata[t_id] += sdata[t_id + 128]; } __syncthreads();}
  if (BLOCK_SIZE >= 128) { if (t_id < 64) { sdata[t_id] += sdata[t_id + 64]; } __syncthreads();}
  if (t_id < 32) {
      if (BLOCK_SIZE >= 64) sdata[t_id] += sdata[t_id + 32];
      if (BLOCK_SIZE >= 32) sdata[t_id] += sdata[t_id + 16];
      if (BLOCK_SIZE >= 16) sdata[t_id] += sdata[t_id + 8];
      if (BLOCK_SIZE >= 8) sdata[t_id] += sdata[t_id + 4];
      if (BLOCK_SIZE >= 4) sdata[t_id] += sdata[t_id + 2];
      if (BLOCK_SIZE >= 2) sdata[t_id] += sdata[t_id + 1]; 
  }
  
  if (t_id == 0) {
      output[blockIdx.x] = sdata[0]; 
  } 
}

int main(int argc, char **argv) {
  int ii;
  wbArg_t args;
  float *hostInput;  // The input 1D list
  float *hostOutput; // The output list
  float *deviceInput;
  float *deviceOutput;
  int numInputElements;  // number of elements in the input list
  int numOutputElements; // number of elements in the output list

  args = wbArg_read(argc, argv);

  wbTime_start(Generic, "Importing data and creating memory on host");
  hostInput =
      (float *)wbImport(wbArg_getInputFile(args, 0), &numInputElements);
  
  if(KERNEL_OPT == 1) {
   numOutputElements = ceil((float)numInputElements / ((float)BLOCK_SIZE));
  }
  else {
     numOutputElements = numInputElements / (BLOCK_SIZE << 1);
     if (numInputElements % (BLOCK_SIZE << 1)) {
       numOutputElements++;
     }
  }
  
  hostOutput = (float *)malloc(numOutputElements * sizeof(float));

  wbTime_stop(Generic, "Importing data and creating memory on host");

  wbLog(TRACE, "The number of input elements in the input is ",
        numInputElements);
  wbLog(TRACE, "The number of output elements in the input is ",
        numOutputElements);

  wbTime_start(GPU, "Allocating GPU memory.");
  //@@ Allocate GPU memory here
  cudaMalloc(&deviceInput, numInputElements * sizeof(float));
  cudaMalloc(&deviceOutput, numOutputElements * sizeof(float));

  wbTime_stop(GPU, "Allocating GPU memory.");

  wbTime_start(GPU, "Copying input memory to the GPU.");
  //@@ Copy memory to the GPU here
  cudaMemcpy(deviceInput, hostInput, numInputElements*sizeof(float), cudaMemcpyHostToDevice);

  wbTime_stop(GPU, "Copying input memory to the GPU.");
  //@@ Initialize the grid and block dimensions here
  // Redcution #1 Kernel
  if (KERNEL_OPT == 1) {
      dim3 gd((numInputElements + BLOCK_SIZE - 1)/BLOCK_SIZE, 1, 1);
      dim3 bd(BLOCK_SIZE, 1, 1);
      total_red1 <<< gd, bd >>> (deviceInput, deviceOutput, numInputElements);
  }
  else if (KERNEL_OPT == 7) {    
      dim3 gd(ceil(1.0*numInputElements/BLOCK_SIZE/2), 1, 1);
      dim3 bd(BLOCK_SIZE, 1, 1);
      total_red7 <<< gd, bd >>> (deviceInput, deviceOutput, numInputElements);
  }
  
  wbTime_start(Compute, "Performing CUDA computation");
  //@@ Launch the GPU Kernel here

  cudaDeviceSynchronize();
  wbTime_stop(Compute, "Performing CUDA computation");

  wbTime_start(Copy, "Copying output memory to the CPU");
  //@@ Copy the GPU memory back to the CPU here
  cudaMemcpy(hostOutput, deviceOutput, numOutputElements*sizeof(float), cudaMemcpyDeviceToHost);

  wbTime_stop(Copy, "Copying output memory to the CPU");

  /********************************************************************
   * Reduce output vector on the host
   * NOTE: One could also perform the reduction of the output vector
   * recursively and support any size input. For simplicity, we do not
   * require that for this lab.
   ********************************************************************/
  for (ii = 1; ii < numOutputElements; ii++) {
    hostOutput[0] += hostOutput[ii];
  }

  wbTime_start(GPU, "Freeing GPU Memory");
  //@@ Free the GPU memory here
  cudaFree(deviceInput);
  cudaFree(deviceOutput);
  

  wbTime_stop(GPU, "Freeing GPU Memory");

  wbSolution(args, hostOutput, 1);

  free(hostInput);
  free(hostOutput);

  return 0;
}
