// Histogram Equalization

#include <wb.h>

#define HISTOGRAM_LENGTH 256
#define THREAD_X 256

// Kernel 1
__global__ void convertFloatToUchar (float *input, unsigned char *output, int size) {
    int i = threadIdx.x + blockIdx.x*blockDim.x;
    
    for (int ii = i; ii < size; ii+=gridDim.x*blockDim.x) {
        if (ii < size) {
            output[ii] = (unsigned char) (255 * input[ii]);
        }
    }
}

// Kernel 2
__global__ void convertRGBtoGrayscale (unsigned char *input, unsigned char *output, 
                                       int h, int w, int channels) {
    int i = threadIdx.x + blockIdx.x*blockDim.x;
    
    for (int ii = i; ii < w*h; ii += gridDim.x*blockDim.x) {
        if (ii < w*h) {
            int rgbOffset = i*channels;
            unsigned char red = input[rgbOffset]; 
            unsigned char green = input[rgbOffset + 1]; 
            unsigned char blue = input[rgbOffset + 2];
            output[i] = (unsigned char) (0.21f*red + 0.71f*green + 0.07f*blue); 
        }
    }
}

// Kernel 3
__global__ void calcHistogram (unsigned char*input, uint*output, int size) {
      const int globalTid = blockIdx.x * blockDim.x + threadIdx.x;
      const int numThreads = blockDim.x * gridDim.x;
      __shared__ uint s_Hist[HISTOGRAM_LENGTH];
      for (int pos = threadIdx.x; pos < HISTOGRAM_LENGTH; pos += blockDim.x) {
          s_Hist[pos] = 0;
      }    
      __syncthreads();

      for (int pos = globalTid; pos < size; pos += numThreads){
          uint data4 = input[pos]; // coalesced
          atomicAdd(s_Hist + ((data4 >> 0) & 0xFFU), 1);
      }
      __syncthreads();
      for (int pos = threadIdx.x; pos < HISTOGRAM_LENGTH; pos += blockDim.x){
              atomicAdd(output + pos, s_Hist[pos]);
      }
}

// Device function for computing CDF
__device__ float p(float x, int w, int h) {
    return x / (w * h);
}  

// Kernel 4
__global__ void calcCDF(uint*array, float*output, int w, int h) {
    __shared__ float scan_array[THREAD_X];
    int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i < 256) {
    	scan_array[threadIdx.x] = p(array[i], w, h);
    }
    __syncthreads();
    
    for (int stride = 1; stride < blockDim.x; stride *= 2) {
    	__syncthreads();
    	if(threadIdx.x >= stride) {
    		scan_array[threadIdx.x] += scan_array[threadIdx.x - stride];
    	}
    }		
    
    if(i < 256) {
    	output[i] = scan_array[i];
    }		
}

__device__ float clamp(float x, float start, float end) {
    return min(max(x, start), end);
}

__device__ float correct_color(unsigned char val, float *cdf){
    return clamp(255.0*((cdf[val]-cdf[0])/(1.0 - cdf[0])), 0.0, 255.0);
}

// Kernel 5
__global__ void calcHistEq (float * cdf, unsigned char* ucharImage, int size) {
    int i = threadIdx.x + blockIdx.x*blockDim.x;
    int numthreads = blockDim.x*gridDim.x;
    
    for (int pos = i; pos < size; pos+=numthreads) {
        ucharImage[pos] = (unsigned char) correct_color(ucharImage[pos], cdf);  
    } 
}

// Kernel 6
__global__ void convertUcharToFloat (unsigned char *input, float *output, int size) {
    int i = threadIdx.x + blockIdx.x*blockDim.x;
    
    for (int ii = i; ii < size; ii+=gridDim.x*blockDim.x) {
        if (ii < size) {
            output[ii] = (float) (input[ii]/255.0);
        }
    }
}

int main(int argc, char **argv) {
  wbArg_t args;
  int imageWidth;
  int imageHeight;
  int imageChannels;
  int img_size;
  int img_len;
  wbImage_t inputImage;
  wbImage_t outputImage;
  float *hostInputImageData;
  float *hostOutputImageData;
  float *deviceInputImage;
  float *deviceOutputImage_k6;
  unsigned char *deviceInputImage_k6;
  unsigned char *deviceOutputImage_k1;
  unsigned char *hostOutputImage_k1;
  unsigned char *deviceInputImage_k2;
  unsigned char *deviceInputImage_k3;
  unsigned char *deviceOutputImage_k2;
  unsigned char *hostOutputImage_k2;
  unsigned char *hostOutputHistEq_k5;
  unsigned char *deviceOutputHistEq_k5;
  unsigned char *deviceInputHistEq_k5;
  uint *deviceOutputHist_k3;
  float *deviceOutputCDF_k4;
  uint *hostOutputHist_k3;
  uint *deviceInputHist_k4;
  float *hostOutputCDF_k4;
  float *deviceInputCDF_k5;
  const char *inputImageFile;
  dim3 bd, gd;

  args = wbArg_read(argc, argv); /* parse the input arguments */

  inputImageFile = wbArg_getInputFile(args, 0);

  wbTime_start(Generic, "Importing data and creating memory on host");
  inputImage = wbImport(inputImageFile);
  imageWidth = wbImage_getWidth(inputImage);
  imageHeight = wbImage_getHeight(inputImage);
  imageChannels = wbImage_getChannels(inputImage);
  outputImage = wbImage_new(imageWidth, imageHeight, imageChannels);
  wbTime_stop(Generic, "Importing data and creating memory on host");
  
  img_size = imageWidth * imageHeight * imageChannels;
  img_len = imageWidth * imageHeight;

  // Pointers to Image 
  hostInputImageData = wbImage_getData(inputImage);
  hostOutputImageData = wbImage_getData(outputImage);
  
  // Allocate GPU Memory
  cudaMalloc(&deviceInputImage, img_size * sizeof(float));
  cudaMalloc(&deviceOutputImage_k1, img_size * sizeof(unsigned char));
  cudaMalloc(&deviceInputImage_k2, img_size * sizeof(unsigned char));
  cudaMalloc(&deviceOutputImage_k2, img_len * sizeof(unsigned char));
  cudaMalloc(&deviceInputImage_k3, img_len * sizeof(unsigned char));
  cudaMalloc(&deviceOutputHist_k3, HISTOGRAM_LENGTH * sizeof(uint));
  cudaMalloc(&deviceInputHist_k4, HISTOGRAM_LENGTH * sizeof(uint));
  cudaMalloc(&deviceOutputCDF_k4, HISTOGRAM_LENGTH * sizeof(float));
  cudaMalloc(&deviceInputCDF_k5, HISTOGRAM_LENGTH * sizeof(float));
  cudaMalloc(&deviceOutputHistEq_k5, img_size * sizeof(unsigned char));
  cudaMalloc(&deviceInputHistEq_k5, img_size * sizeof(unsigned char));
  cudaMalloc(&deviceOutputImage_k6, img_size * sizeof(float));
  cudaMalloc(&deviceInputImage_k6, img_size * sizeof(unsigned char));
  
  // Allocate Host Memory 
  hostOutputImage_k1 = (unsigned char *)malloc(img_size * sizeof(unsigned char));
  hostOutputImage_k2 = (unsigned char *)malloc(img_len * sizeof(unsigned char));
  hostOutputHist_k3 = (uint *)malloc(HISTOGRAM_LENGTH * sizeof(uint));
  hostOutputCDF_k4 = (float *)malloc(HISTOGRAM_LENGTH * sizeof(float));
  hostOutputHistEq_k5 = (unsigned char *)malloc(img_size * sizeof(unsigned char));
  
  
  bd.x = THREAD_X;
  gd.x = ceil((float)img_size/THREAD_X);
  
  // Kernel 1 for conversion from float to uchar
  cudaMemcpy(deviceInputImage, hostInputImageData, img_size * sizeof(float), cudaMemcpyHostToDevice);
  convertFloatToUchar <<< gd, bd >>> (deviceInputImage, deviceOutputImage_k1, img_size); 
  cudaDeviceSynchronize();
  cudaMemcpy(hostOutputImage_k1, deviceOutputImage_k1, img_size * sizeof(unsigned char), cudaMemcpyDeviceToHost);
  
  bd.x = THREAD_X;
  gd.x = ceil((float)img_size/THREAD_X);
  
  // Kernel 2 for conversion from RGB to grayscale
  cudaMemcpy(deviceInputImage_k2, hostOutputImage_k1, img_size * sizeof(unsigned char), cudaMemcpyHostToDevice);
  convertRGBtoGrayscale <<< gd, bd >>> (deviceInputImage_k2, deviceOutputImage_k2, imageHeight, imageWidth, imageChannels); 
  cudaDeviceSynchronize();
  cudaMemcpy(hostOutputImage_k2, deviceOutputImage_k2, img_len * sizeof(unsigned char), cudaMemcpyDeviceToHost);
  
  bd.x = HISTOGRAM_LENGTH;
  gd.x = ceil((float)img_len/HISTOGRAM_LENGTH);

  // Kernel 3 for Histogram Computation
  cudaMemcpy(deviceInputImage_k3, hostOutputImage_k2, img_len * sizeof(unsigned char), cudaMemcpyHostToDevice);
  calcHistogram <<< gd, bd >>> (deviceInputImage_k3, deviceOutputHist_k3, img_len);
  cudaDeviceSynchronize();
  cudaMemcpy(hostOutputHist_k3, deviceOutputHist_k3, HISTOGRAM_LENGTH * sizeof(uint), cudaMemcpyDeviceToHost);
 
  // Kernel 4 for CDF
  cudaMemcpy(deviceInputHist_k4, hostOutputHist_k3, HISTOGRAM_LENGTH * sizeof(uint), cudaMemcpyHostToDevice);
  calcCDF <<< gd, bd >>> (deviceInputHist_k4, deviceOutputCDF_k4, imageWidth, imageHeight);
  cudaDeviceSynchronize();
  cudaMemcpy(hostOutputCDF_k4, deviceOutputCDF_k4, HISTOGRAM_LENGTH * sizeof(float), cudaMemcpyDeviceToHost);
  
  bd.x = HISTOGRAM_LENGTH;
  gd.x = ceil((float)img_size/HISTOGRAM_LENGTH);
  
  // Kernel 5 for Histogram Equalization Function
  cudaMemcpy(deviceInputCDF_k5, hostOutputCDF_k4, HISTOGRAM_LENGTH * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(deviceInputHistEq_k5, hostOutputImage_k1, img_size * sizeof(unsigned char), cudaMemcpyHostToDevice);
  calcHistEq <<< gd, bd >>> (deviceInputCDF_k5, deviceInputHistEq_k5, img_size);  
  cudaDeviceSynchronize();
  cudaMemcpy(hostOutputHistEq_k5, deviceInputHistEq_k5, img_size * sizeof(unsigned char), cudaMemcpyDeviceToHost);
  
  // Kernel 6
  cudaMemcpy(deviceInputImage_k6, hostOutputHistEq_k5, img_size * sizeof(unsigned char), cudaMemcpyHostToDevice);
  convertUcharToFloat <<< gd, bd >>> (deviceInputImage_k6, deviceOutputImage_k6, img_size);  
  cudaDeviceSynchronize();
  cudaMemcpy(hostOutputImageData, deviceOutputImage_k6, img_size * sizeof(float), cudaMemcpyDeviceToHost);
  
  wbSolution(args, outputImage);
  cudaFree(deviceInputImage);
  cudaFree(deviceOutputImage_k1);
  cudaFree(deviceInputImage_k2);
  cudaFree(deviceOutputImage_k2);
  cudaFree(deviceInputImage_k3);
  cudaFree(deviceOutputHist_k3);
  cudaFree(deviceInputHist_k4);
  cudaFree(deviceOutputCDF_k4);
  cudaFree(deviceInputCDF_k5);
  cudaFree(deviceOutputHistEq_k5);
  cudaFree(deviceInputHistEq_k5);
  cudaFree(deviceOutputImage_k6);
  cudaFree(deviceInputImage_k6);

  return 0;
}
