
#include <wb.h>
#define TILE_WIDTH 4

#define wbCheck(stmt)                                                     \
  do {                                                                    \
    cudaError_t err = stmt;                                               \
    if (err != cudaSuccess) {                                             \
      wbLog(ERROR, "Failed to run stmt ", #stmt);                         \
      wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));      \
      return -1;                                                          \
    }                                                                     \
  } while (0)

// Compute C = A * B
__global__ void matrixMultiplyShared(float *A, float *B, float *C,
                                     int numARows, int numAColumns,
                                     int numBRows, int numBColumns,
                                     int numCRows, int numCColumns) {
  // Declare the shared memory variable
  __shared__ float subTileA[TILE_WIDTH][TILE_WIDTH];
  __shared__ float subTileB[TILE_WIDTH][TILE_WIDTH];

  // Index the thread IDs in x and y direction
  int Rows = blockIdx.y*blockDim.y + threadIdx.y; 
  int Cols = blockIdx.x*blockDim.x + threadIdx.x;
  int num_loop = (TILE_WIDTH+numAColumns-1)/TILE_WIDTH;
  float temp_sum = 0;

  // Loop the Matrix A and Matrix B
  for (int i = 0; i < num_loop; ++i) {
    // Add boundary check
    if ((i*TILE_WIDTH + threadIdx.x) < numAColumns && (Rows < numARows)) {
      // Transfer Data from Global Memory to Shared Memory
      subTileA[threadIdx.y][threadIdx.x] = A[Rows*numAColumns + i*TILE_WIDTH + threadIdx.x];         
      subTileB[threadIdx.y][threadIdx.x] = B[(i*TILE_WIDTH+threadIdx.y)*numBColumns + Cols];
    }
    // Wait for threads to fetch all the data
    __syncthreads();   
    
    // Do the multiplication 
    for (int j = 0; j < TILE_WIDTH; ++j) {
      temp_sum = temp_sum + subTileA[threadIdx.y][j]*subTileB[j][threadIdx.x];
    }      
    // Wait for threads to fetch all the data
    __syncthreads();   
  }
  if ((Rows < numCRows) && (Cols < numCColumns)) 
    // Copy the results back to the Global Memory
    C[Rows*numCColumns + Cols] = temp_sum;  
}

int main(int argc, char **argv) {
  wbArg_t args;
  float *hostA; // The A matrix
  float *hostB; // The B matrix
  float *hostC; // The output C matrix
  float *deviceA;
  float *deviceB;
  float *deviceC;
  int numARows;    // number of rows in the matrix A
  int numAColumns; // number of columns in the matrix A
  int numBRows;    // number of rows in the matrix B
  int numBColumns; // number of columns in the matrix B
  int numCRows;    // number of rows in the matrix C (you have to set this)
  int numCColumns; // number of columns in the matrix C (you have to set
                   // this)
  int sizeA;
  int sizeB;
  int sizeC;

  args = wbArg_read(argc, argv);

  wbTime_start(Generic, "Importing data and creating memory on host");
  hostA = (float *)wbImport(wbArg_getInputFile(args, 0), &numARows,
                            &numAColumns);
  hostB = (float *)wbImport(wbArg_getInputFile(args, 1), &numBRows,
                            &numBColumns);
  //@@ Set numCRows and numCColumns
  numCRows = numARows;
  numCColumns = numBColumns;
  
  sizeA = numARows * numAColumns * sizeof(float);
  sizeB = numBRows * numBColumns * sizeof(float);
  sizeC = numCRows * numCColumns * sizeof(float);
  //@@ Allocate the hostC matrix
  hostC = (float *)malloc(sizeC);

  wbTime_stop(Generic, "Importing data and creating memory on host");

  wbLog(TRACE, "The dimensions of A are ", numARows, " x ", numAColumns);
  wbLog(TRACE, "The dimensions of B are ", numBRows, " x ", numBColumns);

  wbTime_start(GPU, "Allocating GPU memory.");
  //@@ Allocate GPU memory here
  cudaMalloc(&deviceA, sizeA);
  cudaMalloc(&deviceB, sizeB);
  cudaMalloc(&deviceC, sizeC);

  wbTime_stop(GPU, "Allocating GPU memory.");

  wbTime_start(GPU, "Copying input memory to the GPU.");
  //@@ Copy memory to the GPU here
  cudaMemcpy(deviceA, hostA, sizeA, cudaMemcpyHostToDevice);
  cudaMemcpy(deviceB, hostB, sizeB, cudaMemcpyHostToDevice);

  wbTime_stop(GPU, "Copying input memory to the GPU.");

  //@@ Initialize the grid and block dimensions here
  dim3 threadsPerBlock(TILE_WIDTH,TILE_WIDTH);
  dim3 blocksPerGrid((numCColumns+TILE_WIDTH-1)/TILE_WIDTH,(numCRows+TILE_WIDTH-1)/TILE_WIDTH);

  wbTime_start(Compute, "Performing CUDA computation");
  //@@ Launch the GPU Kernel here
  matrixMultiplyShared <<< blocksPerGrid, threadsPerBlock >>> (deviceA, deviceB, deviceC, numARows, numAColumns, numBRows, numBColumns, numCRows, numCColumns);

  cudaDeviceSynchronize();
  wbTime_stop(Compute, "Performing CUDA computation");

  wbTime_start(Copy, "Copying output memory to the CPU");
  //@@ Copy the GPU memory back to the CPU here
  cudaMemcpy(hostC, deviceC, sizeC, cudaMemcpyDeviceToHost);

  wbTime_stop(Copy, "Copying output memory to the CPU");

  wbTime_start(GPU, "Freeing GPU Memory");
  //@@ Free the GPU memory here
  cudaFree(deviceA);
  cudaFree(deviceB);
  cudaFree(deviceC);

  wbTime_stop(GPU, "Freeing GPU Memory");

  wbSolution(args, hostC, numCRows, numCColumns);

  free(hostA);
  free(hostB);
  free(hostC);

  return 0;
}
