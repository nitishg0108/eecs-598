#ifndef MXNET_OPERATOR_NEW_FORWARD_CUH_
#define MXNET_OPERATOR_NEW_FORWARD_CUH_

#include <mxnet/base.h>

namespace mxnet
{
namespace op
{

#define THREAD_X 16
__constant__ float k[24*12*7*7]; // M*C*K*K

__global__ void forward_kernel(float *y, const float *x, const int B, const int M, const int C, const int H, const int W, const int K)
{
    const int H_out = H - K + 1;
    const int W_out = W - K + 1;

#define y4d(i3, i2, i1, i0) y[(i3) * (M * H_out * W_out) + (i2) * (H_out * W_out) + (i1) * (W_out) + i0]
#define x4d(i3, i2, i1, i0) x[(i3) * (C * H * W) + (i2) * (H * W) + (i1) * (W) + i0]
#define k4d(i3, i2, i1, i0) k[(i3) * (C * K * K) + (i2) * (K * K) + (i1) * (K) + i0]

    int b = blockIdx.x;
    int m = blockIdx.y;
    int h_base = (blockIdx.z / ((H_out-1)/THREAD_X+1) * (THREAD_X));
    int w_base = (blockIdx.z % ((W_out-1)/THREAD_X+1) * (THREAD_X));
    int h = h_base + threadIdx.x;
    int w = w_base + threadIdx.y; 
    
    float temp;
    extern __shared__ float smem[];
    float *X_shared = &smem[0];
    float *W_shared = &smem[(THREAD_X + K - 1) * (THREAD_X + K - 1)];
    
    temp = 0;
    for (int c = 0; c < C; c++) {       
        
        // Copy W to Shared Memory
        if ((threadIdx.x < K) && (threadIdx.y < K)) {
            W_shared[threadIdx.x * K + threadIdx.y] = k4d(m,c,threadIdx.x, threadIdx.y);
        } 
        __syncthreads();
        
        int it = threadIdx.x*(THREAD_X + K - 1); 
        // Copy X to shared Memory
        for (int i = h; i < h_base + THREAD_X + K - 1; i+=THREAD_X, it+=(THREAD_X + K - 1)*(THREAD_X)) {
            for (int j = w; j < w_base + THREAD_X + K - 1; j+=THREAD_X) {
                X_shared[it + (j-w_base)] = x4d(b,c,i,j);
            }
        }
        __syncthreads();
        
        it = threadIdx.x*(THREAD_X + K - 1); 
        for (int p = 0; p < K*K; p+=K, it+= (THREAD_X + K - 1)) { // KxK filter
            for (int q = 0; q < K; q++)
                temp += X_shared[it + threadIdx.y + q] * W_shared[p+q];
        }
        __syncthreads();
    }
      
    if ((h < H_out) && (w < W_out)) 
        y4d(b,m,h,w) = temp;

#undef y4d
#undef x4d
#undef k4d
}


/* 
   This function is called by new-inl.h
   Any code you write should be executed by this function.
   We only expect the float version of the operator to be called, so here we specialize with only floats.
*/
template <>
void forward<gpu, float>(mshadow::Tensor<gpu, 4, float> &y, const mshadow::Tensor<gpu, 4, float> &x, const mshadow::Tensor<gpu, 4, float> &w)
{

    const int B = x.shape_[0];
    const int M = y.shape_[1]; // num_filter
    const int C = x.shape_[1];
    const int H = x.shape_[2];
    const int W = x.shape_[3];
    const int K = w.shape_[3];
    const int H_out = H - K + 1;
    const int W_out = W - K + 1;
    const int W_grid = ((W_out - 1) / THREAD_X+1);
    const int H_grid = ((H_out - 1) / THREAD_X+1);
    const int z = W_grid * H_grid;
    
    dim3 blockDim(THREAD_X, THREAD_X, 1);
    dim3 gridDim(B, M, z);
    
    cudaMemcpyToSymbol(k, w.dptr_, M*C*K*K*sizeof(float));    

    MSHADOW_CUDA_CALL(cudaDeviceSynchronize());
    size_t smem_size = sizeof(float)*((THREAD_X+K-1)*(THREAD_X+K-1) + K*K);
    forward_kernel<<<gridDim, blockDim, smem_size>>>(y.dptr_, x.dptr_, B, M, C, H, W, K);
    MSHADOW_CUDA_CALL(cudaDeviceSynchronize());
}

/* 
    This tells mxnet how to do an op when it's not a float.
    This is not used in the project
*/
template <typename gpu, typename DType>
void forward(mshadow::Tensor<gpu, 4, DType> &y, const mshadow::Tensor<gpu, 4, DType> &x, const mshadow::Tensor<gpu, 4, DType> &w)
{
    assert(0 && "No forward implementation for other datatypes needed");
}
}
}

#endif
