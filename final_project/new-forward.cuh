#ifndef MXNET_OPERATOR_NEW_FORWARD_CUH_
#define MXNET_OPERATOR_NEW_FORWARD_CUH_

#include <mxnet/base.h>

namespace mxnet
{
namespace op
{

#define THREAD_X 16
__constant__ float k[14112]; // M*C*K*K

__global__ void forward_kernel(float *y, const float *x, const int M, const int C, const int H, const int W)
{
    int H_out = H - 6;
    int W_out = W - 6;
    int H_W_out = H_out*W_out;
    int H_W = H*W;

#define y4d(i3, i2, i1, i0) y[(H_W_out)*(M*i3 + i2) + (i1) * (W_out) + i0]
#define x4d(i3, i2, i1, i0) x[(H_W)*(C*i3+i2) + (i1) * (W) + i0]
#define k4d(i3, i2, i1, i0) k[(49)*(C*i3 + i2) + (i1) * (7) + i0]

    int b = blockIdx.x;
    int m = blockIdx.y;
    int h_base = (blockIdx.z / ((H_out-1)/16+1) * (THREAD_X));
    int w_base = (blockIdx.z % ((W_out-1)/16+1) * (THREAD_X));
    int h = h_base + threadIdx.y;
    int w = w_base + threadIdx.x;
    int mul = threadIdx.x*22; 
    
    float temp;
    extern __shared__ float smem[];
    float *X_shared = &smem[0];
    float *W_shared = &smem[484];
    
    int it_r = mul -w_base; 
    int it_id = mul + threadIdx.y;
    int w_s = threadIdx.x*7 + threadIdx.y; 
    temp = 0;
    for (int c = 0; c < C; c++) {       
        
        if ((threadIdx.x < 7) && (threadIdx.y < 7)) {
            W_shared[w_s] = k[(49)*(C*m + c) + w_s];
        } 
        
        int it = 0;
        for (int i = h; i < h_base + 22; i+=THREAD_X, it+=352) {
            for (int j = w; j < w_base + 22; j+=THREAD_X) {
                X_shared[it +it_r+ j] = x4d(b,c,i,j);
            }
        }
        __syncthreads();
        
        temp += X_shared[it_id] * W_shared[0] + X_shared[it_id + 1] * W_shared[1]+X_shared[it_id + 2] * W_shared[2]+X_shared[it_id + 3] * W_shared[3] + X_shared[it_id  + 4] * W_shared[4] + X_shared[it_id + 5] * W_shared[5] + X_shared[it_id + 6] * W_shared[6]+ X_shared[it_id+22] * W_shared[7] + X_shared[it_id+23] * W_shared[8]+X_shared[it_id+24] * W_shared[9]+X_shared[it_id+25] * W_shared[10] + X_shared[it_id+26] * W_shared[11] + X_shared[it_id+27] * W_shared[12] + X_shared[it_id+28] * W_shared[13]+X_shared[it_id+44] * W_shared[14] + X_shared[it_id+45] * W_shared[15]+X_shared[it_id+46] * W_shared[16]+X_shared[it_id+47] * W_shared[17] + X_shared[it_id+48] * W_shared[18] + X_shared[it_id+49] * W_shared[19] + X_shared[it_id+50] * W_shared[20]+X_shared[it_id+66] * W_shared[21] + X_shared[it_id+67] * W_shared[22]+X_shared[it_id+68] * W_shared[23]+X_shared[it_id+69] * W_shared[24] + X_shared[it_id+70] * W_shared[25] + X_shared[it_id+71] * W_shared[26] + X_shared[it_id+72] * W_shared[27]+X_shared[it_id+88] * W_shared[28] + X_shared[it_id+89] * W_shared[29]+X_shared[it_id+90] * W_shared[30]+X_shared[it_id+91] * W_shared[31] + X_shared[it_id+92] * W_shared[32] + X_shared[it_id+93] * W_shared[33] + X_shared[it_id+94] * W_shared[34]+X_shared[it_id+110] * W_shared[35] + X_shared[it_id+111] * W_shared[36]+X_shared[it_id+112] * W_shared[37]+X_shared[it_id+113] * W_shared[38] + X_shared[it_id+114] * W_shared[39] + X_shared[it_id+115] * W_shared[40] + X_shared[it_id+116] * W_shared[41]+X_shared[it_id+132] * W_shared[42] + X_shared[it_id+133] * W_shared[43]+X_shared[it_id+134] * W_shared[44]+X_shared[it_id+135] * W_shared[45] + X_shared[it_id+136] * W_shared[46] + X_shared[it_id+137] * W_shared[47] + X_shared[it_id+138] * W_shared[48];

 __syncthreads();
}      
    if ((h < H_out) && (w < W_out)) 
        y4d(b,m,h,w) = temp;

#undef y4d
#undef x4d
#undef k4d
}


/* 
   This function is called by new-inl.h
   Any code you write should be executed by this function.
   We only expect the float version of the operator to be called, so here we specialize with only floats.
*/
template <>
void forward<gpu, float>(mshadow::Tensor<gpu, 4, float> &y, const mshadow::Tensor<gpu, 4, float> &x, const mshadow::Tensor<gpu, 4, float> &w)
{

    const int B = x.shape_[0];
    const int M = y.shape_[1]; // num_filter
    const int C = x.shape_[1];
    const int H = x.shape_[2];
    const int W = x.shape_[3];
    const int K = w.shape_[3];
    const int H_out = H - K + 1;
    const int W_out = W - K + 1;
    const int W_grid = ((W_out - 1) / THREAD_X+1);
    const int H_grid = ((H_out - 1) / THREAD_X+1);
    const int z = W_grid * H_grid;
    
    dim3 blockDim(THREAD_X, THREAD_X, 1);
    dim3 gridDim(B, M, z);
    
    cudaMemcpyToSymbol(k, w.dptr_, M*C*K*K*sizeof(float));    

    MSHADOW_CUDA_CALL(cudaDeviceSynchronize());
    size_t smem_size = sizeof(float)*((THREAD_X+K-1)*(THREAD_X+K-1) + K*K);
    forward_kernel<<<gridDim, blockDim, smem_size>>>(y.dptr_, x.dptr_, M, C, H, W);
    MSHADOW_CUDA_CALL(cudaDeviceSynchronize());
}

/* 
    This tells mxnet how to do an op when it's not a float.
    This is not used in the project
*/
template <typename gpu, typename DType>
void forward(mshadow::Tensor<gpu, 4, DType> &y, const mshadow::Tensor<gpu, 4, DType> &x, const mshadow::Tensor<gpu, 4, DType> &w)
{
    assert(0 && "No forward implementation for other datatypes needed");
}
}
}

#endif
